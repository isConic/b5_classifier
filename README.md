# b5_classifier

## Installation
```bash
pip install -r requirements.txt
```

## Training
Run this at least once before using. It creates the model.   
```bash
python big_five_classifier/train_classifier.py
```

## Usage

### import and create model
```python
from big_five_classifier.classifier import BFiveClassifier
from big_five_classifier.train_classifier import Model

classifer = BFiveClassifier()

```

### classify a sentence

```python
text = """Rabies is truly terrifying.
    I tried explaining to him my fear of raccoons and how rabies plays
    into that he had that terrible racoon picture for like 25 years
    """
    
scores = classifer.classify([text])
print(scores)

```

```json
{
  "OPN": 74.93909223499938,
  "CON": 57.518077852895566,
  "EXT": 52.180740299093266,
  "AGR": 62.08119860534488,
  "NEU": 41.771277981723415
}
```

### classify multiple sentences
```python
multiple_text = ["Rabies is truly terrifying.",
				 "I tried explaining to him my fear of raccoons and how rabies plays into that he had that ",
				 "he had that terrible racoon picture for like 25 years"]
scores = classifer.classify([multiple_text])
print(scores)
```

```json
{
 "OPN": 72.1697831120692,
 "CON": 56.4125704698901,
 "EXT": 57.686703725243625,
 "AGR": 57.473226799122976,
 "NEU": 39.56976571126564,
 }
```

### create a radar plot
```python

radar_path = classifer.plot_radar([text], output_path= "example.png", strength= 0.5)
print(radar_path)

```
```json
example.png
```

![](example.png)

### create a radar plot with multiple plots

```python
multiple_text = ["Rabies is truly terrifying.",
				 "I tried explaining to him my fear of raccoons and how rabies plays into that he had that ",
				 "he had that terrible racoon picture for like 25 years"]

radar_path = classifer.plot_radar(multiple_text, output_path= "example_multiple.png", strength= 0.5)

```

```python
example_multiple.png
```

![](example_multiple.png)