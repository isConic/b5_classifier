from big_five_classifier.classifier import BFiveClassifier
from big_five_classifier.train_classifier import Model

classifer = BFiveClassifier()

text = """Rabies is truly terrifying.
    I tried explaining to him my fear of raccoons and how rabies plays
    into that he had that terrible racoon picture for like 25 years
    """

scores = classifer.classify([text])
print(scores)

# {'OPN': 77.40289833988808, 'CON': 58.23030735356956, 'EXT': 52.50897018885936, 'AGR': 60.50543288873364, 'NEU': 39.20589013773118}

radar_path = classifer.plot_radar([text], output_path= "example.png", strength= 0.5)
print(radar_path)


multiple_text = ["Rabies is truly terrifying.",
				 "I tried explaining to him my fear of raccoons and how rabies plays into that he had that ",
				 "he had that terrible racoon picture for like 25 years"]

radar_path = classifer.plot_radar(multiple_text, output_path= "example_multiple.png", strength= 0.5)
scores = classifer.classify(multiple_text)
print(scores)