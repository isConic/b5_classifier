#### How to train

Use the model.py

`py model.py`

This will create 5 models for each of the OCEAN letters, these later can be used 
via BFiveClassifier, note that if there are no models trained, BFiveClassifier will throw an error and exit