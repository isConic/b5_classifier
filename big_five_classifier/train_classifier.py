import pickle
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
import pathlib

class Model():
    def __init__(self):
        self.rfr = RandomForestRegressor(bootstrap=True,
         max_features='sqrt',
         min_samples_leaf=1,
         min_samples_split=2,
         n_estimators= 200)
        self.rfc = RandomForestClassifier(max_features='sqrt', n_estimators=110)
        self.tfidf = TfidfVectorizer(stop_words='english', strip_accents='ascii')

    def fit(self, X, y, regression=True):
        X = self.tfidf.fit_transform(X)
        if regression:
            self.rfr = self.rfr.fit(X, y)
        else:
            self.rfc = self.rfc.fit(X, y)

    def predict(self, X, regression=True):
        X = self.tfidf.transform(X)
        if regression:
            return self.rfr.predict(X)
        else:
            return self.rfc.predict(X)

    def predict_proba(self, X, regression=False):
        X = self.tfidf.transform(X)
        if regression:
            raise ValueError('Cannot predict probabilites of a regression!')
        else:
            return self.rfc.predict_proba(X)



class DataPrep():
    def __init__(self, path:str= "./"):
        self.trait_cat_dict = {
            'O': 'cOPN',
            'C': 'cCON',
            'E': 'cEXT',
            'A': 'cAGR',
            'N': 'cNEU',
            'OPN': 'cOPN',
            'CON': 'cCON',
            'EXT': 'cEXT',
            'AGR': 'cAGR',
            'NEU': 'cNEU',
            'Openness': 'cOPN',
            'Conscientiousness': 'cCON',
            'Extraversion': 'cEXT',
            'Agreeableness': 'cAGR',
            'Neuroticism': 'cNEU'
            }
        self.trait_score_dict = {
            'O': 'sOPN',
            'C': 'sCON',
            'E': 'sEXT',
            'A': 'sAGR',
            'N': 'sNEU',
            'OPN': 'sOPN',
            'CON': 'sCON',
            'EXT': 'sEXT',
            'AGR': 'sAGR',
            'NEU': 'sNEU',
            'Openness': 'sOPN',
            'Conscientiousness': 'sCON',
            'Extraversion': 'sEXT',
            'Agreeableness': 'sAGR',
            'Neuroticism': 'sNEU'
            }
        self.path = pathlib.Path(path)

    def prep_data(self, trait, regression=False, model_comparison=False):
        df_status = self.prep_status_data()
        # df_essay = self.prep_essay_data()

        tfidf = TfidfVectorizer(stop_words='english', strip_accents='ascii')
        # Include other features with tfidf vector
        other_features_columns = [
            'NETWORKSIZE',
            'BETWEENNESS',
            'NBETWEENNESS',
            'DENSITY',
            'BROKERAGE',
            'NBROKERAGE',
            'TRANSITIVITY'
        ]
        # result = tfidf.fit_transform(df_status['STATUS']).todense()

        # If need data to compare models
        if model_comparison:
            X = tfidf.fit_transform(df_status['STATUS'])
            # X = np.nan_to_num(np.column_stack((result, df_status[other_features_columns])))
        # Data to fit production model
        else:
            X = df_status['STATUS']

        if regression:
            y_column = self.trait_score_dict[trait]
        else:
            y_column = self.trait_cat_dict[trait]
        y = df_status[y_column]

        return X, y


    def prep_status_data(self):
        df = pd.read_csv(self.path/'data/data.csv', encoding="ISO-8859-1")
        df = self.convert_traits_to_boolean(df)
        return df

    def convert_traits_to_boolean(self, df):
        trait_columns = ['cOPN', 'cCON', 'cEXT', 'cAGR', 'cNEU']
        d = {'y': True, 'n': False}

        for trait in trait_columns:
            df[trait] = df[trait].map(d)

        return df

def train_model(path:str, traits:list=['OPN', 'CON', 'EXT', 'AGR', 'NEU']):
    path = pathlib.Path(str(path))

    model = Model()

    for trait in traits:
        dp = DataPrep(path = path)
        X_regression, y_regression = dp.prep_data(trait, regression=True, model_comparison=False)
        X_categorical, y_categorical = dp.prep_data(trait, regression=False, model_comparison=False)
        print('Fitting trait ' + trait)
        print('::::Regression Model: ')
        model.fit(X_regression, y_regression, regression=True)
        print('Done!')

        print('::::Categorical Model: ')
        model.fit(X_categorical, y_categorical, regression=False)
        print('Done!')

        with open(path/"models"/f"{trait}_model.pkl", 'wb') as fs:
            # Write the model to a file.
            pickle.dump(model, fs)

if __name__ == '__main__':
    import pathlib
    path = pathlib.Path(__file__).parent.absolute()
    train_model(path)