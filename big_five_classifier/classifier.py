import pickle
import pathlib
from big_five_classifier.train_classifier import Model,DataPrep
import matplotlib.pyplot as plt
from math import pi
from typing import List, Dict
import time
import numpy as np
from typing import List

class BFiveClassifier:
    def __init__(self):
        self.traits = ['OPN', 'CON', 'EXT', 'AGR', 'NEU']
        self.models = {}
        # these values are extracted from the dataset
        # need these values to calculate the % of each trait
        self.ranges = { "OPN" : { "Min" : 2.25, "Max" : 5.0 }, "CON" : {"Min" : 1.45, "Max" : 5.0}, "AGR" : {"Min": 1.65,"Max" : 5.0},"NEU" : {"Min" : 1.25,"Max" : 4.75},"EXT" : {"Min" : 1.33,"Max" : 5.0} }

        # load models
        for trait in self.traits:
            path = pathlib.Path(__file__).parent.absolute()
            model_path = path/"models"/ f'{trait}_model.pkl'
            if model_path.exists():
                with open(model_path, 'rb') as f:
                    self.models[trait] = pickle.load(f)
            else:
                raise ValueError(f"Error loading classifier, {model_path} doesn't exists")

    # takes text string as an input and returns a dictionary with percentage values of traits
    def classify(self, text:List[str]):
        preds = {}
        for k in self.traits:
            trait_preds= self.models[k].predict(text)
            # predicting a list of str will return a list of prediction for each string
            # let's get the average of all the predictions
            avr_trait_pred = sum(trait_preds) / len(trait_preds)

            # percentage calculation
            preds[k] = ((avr_trait_pred-self.ranges[k]["Min"]) / (self.ranges[k]["Max"]-self.ranges[k]["Min"])) *100
        return preds


    def plot_radar(self, sentences:List[str], output_path = None, strength=1.0) -> str:
        if output_path is None:
            output_path = f"./{str(time.time()).replace('.','_')}.png"
        elif ".png" not in output_path:
            output_path+= str(time.time()).replace('.','_') + ".png"

        vals = [*map(self.classify, map(lambda x: [x], sentences))]
        fig = plt.figure(figsize=(6, 6))
        fig.patch.set_facecolor('#FFFFFF')
        ax = plt.subplot(polar="True")

        categories = ["Openness",
                      "Conscientiousness",
                      "Extraversion",
                      "Agreeableness",
                      "Nueroticism", ]

        N = len(categories)

        for val in vals:
            values = [val["OPN"],
                      val["CON"],
                      val["EXT"],
                      val["AGR"],
                      val["NEU"], ]

            values += values[:1]

            angles = [n / float(N) * 2 * pi for n in range(N)]
            angles += angles[:1]

            plt.fill(angles, values, alpha=strength / len(vals), color="blue")

        plt.xticks(angles[:-1], categories)

        ax.set_rlabel_position(0)
        plt.yticks([0, 20, 40, 60, 80, 100], color="grey", size=10)
        plt.ylim(0, 100)
        plt.savefig(output_path)

        return str(pathlib.Path(output_path))

if __name__ == "__main__":
    # a test
    classifier = BFiveClassifier()
    print(classifier.classify("""I don’t understand why we must do things in this world, why we must have friends and aspirations, hopes and dreams. Wouldn’t it be better to retreat to a faraway corner of the world, where all its noise and complications would be heard no more? Then we could renounce culture and ambitions; we would lose everything and gain nothing; for what is there to be gained from this world?"""))

    print(classifier.plot_radar(["I'm having a good day today", "Things are simply horrible"], strength = 0.6))
