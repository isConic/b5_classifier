# For relative imports to work in Python
import os, sys; sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from .train_classifier import Model